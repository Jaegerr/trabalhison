/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Jango
 */
public class ClassCampos {

    public ClassCampos(String nomeCampo, String tipo) {
        this.nomeCampo = nomeCampo;
        this.tipo = tipo;
    }
    private String nomeCampo ;
    private String tipo;

    
    public String getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
}
