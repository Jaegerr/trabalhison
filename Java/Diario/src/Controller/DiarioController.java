/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.DAOAluno;
import Model.Aluno;
import java.sql.SQLException;
/**
 *
 * @author Jango
 */
public class DiarioController {
    private DAOAluno daoAluno;

    public DiarioController() throws SQLException {
        daoAluno = new DAOAluno();
    }
   public boolean SalvarAluno(String nome, String turma, String presenca) throws SQLException{
       return daoAluno.SalvarAluno(new Aluno(nome,turma,presenca));
   }
   
    
}
