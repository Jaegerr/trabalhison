/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Jango
 */
public class DAOAluno {
    private Connection connection;
    
    public DAOAluno () throws SQLException{
        
        connection = ConnectionDatabase.getConnection();
    }
    
    public boolean SalvarAluno(Aluno aluno) throws SQLException{
        int retorno=0;
        long var=0;
        
        String sql= "INSERT INTO Aluno(nome,turma,presenca) VALUES(?,?,?)";
        
        PreparedStatement prd = connection.prepareStatement(sql);
        
        prd.setString(1, aluno.getNome());
        prd.setString(2, aluno.getTurma());
        prd.setString(3, aluno.getPresenca());
        
        retorno = prd.executeUpdate();
        
        if(retorno !=0){
            return true;
        }
        else{
            return false;
        }
    }
}
