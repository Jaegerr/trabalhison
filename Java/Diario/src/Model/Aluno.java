/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Jango
 */
public class Aluno {

    /**
     * @param args the command line arguments
     */
   private String nome;
   private String turma;
   private String presenca;

    public Aluno(String nome, String turma, String presenca) {
        this.nome = nome;
        this.turma = turma;
        this.presenca = presenca;
    }
   
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getPresenca() {
        return presenca;
    }

    public void setPresença(String presenca) {
        this.presenca = presenca;
    }
   
   
   
    
}
